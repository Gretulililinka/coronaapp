import kivy

kivy.require('2.0.0')

from kivy.app import App
from kivy.properties import ListProperty
from kivy.clock import Clock

from kivy.uix.boxlayout import BoxLayout
from kivy.uix.screenmanager import ScreenManager, Screen

# qrkode neni součást kivy si to musíme doinstalovat zlášť
from kivy_garden.qrcode import QRCodeWidget

import random

# rgba barvičky pro zelenou a žlutou na barvení kivy věcí 
ZLUTA = [0.75, 0.75, 0, 1]
ZELENA = [0, 0.75, 0, 1]

# ukazkovej z prikladu tady hele https://github.com/eu-digital-green-certificates/dgc-testdata/blob/main/AT/2DCode/raw/1.json
# sem si mužete stčit svuj vopravdickej aby byl v bezpečí před vládní appkou a pak mužete dát dopryč to dělání náhodnejch kódů který je tam jenom z testovacích důvodů ;D ;D ;D ;D ;D ;D
koronakod="HC1:NCFOXN%TS3DH3ZSUZK+.V0ETD%65NL-AH-R6IOO6+IUKRG*I.I5BROCWAAT4V22F/8X*G3M9JUPY0BX/KR96R/S09T./0LWTKD33236J3TA3M*4VV2 73-E3GG396B-43O058YIB73A*G3W19UEBY5:PI0EGSP4*2DN43U*0CEBQ/GXQFY73CIBC:G 7376BXBJBAJ UNFMJCRN0H3PQN*E33H3OA70M3FMJIJN523.K5QZ4A+2XEN QT QTHC31M3+E32R44$28A9H0D3ZCL4JMYAZ+S-A5$XKX6T2YC 35H/ITX8GL2-LH/CJTK96L6SR9MU9RFGJA6Q3QR$P2OIC0JVLA8J3ET3:H3A+2+33U SAAUOT3TPTO4UBZIC0JKQTL*QDKBO.AI9BVYTOCFOPS4IJCOT0$89NT2V457U8+9W2KQ-7LF9-DF07U$B97JJ1D7WKP/HLIJLRKF1MFHJP7NVDEBU1J*Z222E.GJF67Z JA6B.38O4BH*HB0EGLE2%V -3O+J3.PI2G:M1SSP2Y3D38-G9C+Q3OT/.J1CDLKOYUV5C3W1A:75S4LB:6REPKM3ZYO4+QDNDLT2*ESLIH"


    
# funkce na vygenerování něčeho co jako vypadá jako vopravdickej koronakód :D :D
def nahodny_koronakod():
    global koronakod
    # všechny možný znaky který muže mit něco skovanýho v base45
    znaky = ' $%-+*/.ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
    # koronaqrkódy maj takovejdle prefix :O :O
    koronakod = 'HC1:'
    # a sou 604 znaků dlouhý (i stim prefixem)
    for i in range(600):
        koronakod += random.choice(znaky)
        

# bude se nám to skládat jakoby ze dvou různejch vobrazovek
# na hlavní vobrazovce budem mit ukazování toho našeho qrkódu a na druhý nějaký takový jakože 
# settings který zatim nedělaj nic jinýho než čudlikem tamten uplně náhodnej pseudokoronaqrkód :D :D
# dědíme z oběktu screen by sme to mohli šoupnout do screen manageru kterej nám bude ty vobrazovky přepínat pak
class HlavniObrazovka(Screen):
    
    # takle mužem kivy věcem přidělávat atributy
    # de pak nato věšet eventy třeba (to tady neni použitý) :O ;D
    # a používat to v *.kv souboru (to tady je použitý :D :D :D :D)
    statusova_barva = ListProperty(ZLUTA)
    
    # konstruktor
    def __init__(self, **kwargs):
        
        # nastavíme si jednorázovej časovač na dvě sekundy kterej nám pak zavolá metodu 'my_callback'
        Clock.schedule_once(self.my_callback, 2)
        
        # volání konstruktora rodiče
        super(HlavniObrazovka, self).__init__(**kwargs)
        
    # screen umí dělat eventy když je vodstraňovaná/vstupuje na scénu
    # on pre enter je vypálenej eště než začne samotná animace vstupování vobrazovky
    # (je tam taky event on_enter kterej je vypálenej až po)
    # tady jenom zase zapnem ten dvousekundovej timer kterej už máme v konstruktoru
    def on_pre_enter(self):
        Clock.schedule_once(self.my_callback, 2)
        
    # vždycky když hlavní vobrazovka vodchází ze scény tak jakoby zežloutne po tom zmizení
    # je to proto aby to po vrácení se vypadalo jakože to je supr bezpečný a furt se to vověřuje :D :D
    def on_leave(self):
        self.zezloutnout()
            
    # tady je ten callback dvousekundovej
    # jenom strčí do widgetu s qr kódem náš koronakód a nechá zezelenat titulek + status text
    def my_callback(self, dt):
        qr = self.ids['qr']
        qr.data = koronakod
        self.zezelenat()
         
    # přepnem barvu pozadí titulku+status textu na žlutou
    # jako status text dáme 'connecting...' jakože se to někam asi připojuje :D :D
    # nastavíme atribut data qr widgetu na prázdnej string to se na tom widgetu projeví
    # tak že se tam začně točit nějakej takovej hnusnej čekací gif v mrňavím rozlišení
    # žeto je hnusný neva aspoň to vypadá jako něco co vyrobila státní správa nějaká :D
    def zezloutnout(self):
        self.statusova_barva = ZLUTA
        self.ids['label_status'].text='Connecting...'
        self.ids['qr'].data=''
      
    # to samý co zežloutnout akorátže se zelenou barvičkou
    # a statustextem 'immune' :D ;D
    # do qrkódu strkáme data takže by měl děsně rychle zmiznout ten točicí gif
    def zezelenat(self):
        self.statusova_barva = ZELENA
        self.ids['label_status'].text='Immune'
        self.ids['qr'].data = koronakod
        
    # metoda co dělá že přepíná mezi žlutou a zelenou
    def prepnout_barvu(self):
        if self.statusova_barva == ZLUTA:
            self.zezelenat()
        else:
            self.zezloutnout()

# druhá vobrazovka má jenom jednu custom metodu ato na vygenerování toho novýho qrkoronakódu
class Nastaveni(Screen):
    def cudlik_update_cert(self):
        nahodny_koronakod()
        
            
# název přiloženýho *.kv souboru musí bejt stejnej s tim co je tady napsaný před tim 'App' v méně třídy :O :O
# takže kdyby se to třeba jako menovalo teckaApp tak vodpovídací *.kv soubor se musí menovat 'tecka.kv' třeba :O ;D
# (artelnativní způsob jestli někomu děsně vadí ten kv soubor je to co se piše do *.kv napsat do stringu a pak to strčit do Build.load_string hele https://kivy.org/doc/stable/api-kivy.lang.builder.html#kivy.lang.builder.BuilderBase.load_string )
class certifikatApp(App):
    def build(self):
        
        # vyrobíme si screenmanager do kterýho nastráme instance těch našich vobrazovek
        # nastavíme jim ňáký ména pomocí kterejch mezi nima jakoby budem přepínat furt
        sm = ScreenManager()
        sm.add_widget(HlavniObrazovka(name='hlavni'))
        sm.add_widget(Nastaveni(name='settings'))
        
        # nastavíme nějakej uplně náhodnej koronakód
        nahodny_koronakod()
        
        return sm
    

# pythoní main funkce 
if __name__ == '__main__':
    certifikatApp().run()
 

