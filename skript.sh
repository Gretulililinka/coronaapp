#!/usr/bin/env bash

archs=( armeabi-v7a arm64-v8a x86 x86_64 )
for i in "${archs[@]}"
do
   sed -i "s/android\.arch.*/android.arch = $i/" ./buildozer.spec
   buildozer android debug
   buildozer android release
done
